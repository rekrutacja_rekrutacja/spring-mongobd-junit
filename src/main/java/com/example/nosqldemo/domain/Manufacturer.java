package com.example.nosqldemo.domain;

import java.util.List;

import org.bson.types.ObjectId;

public class Manufacturer {

	private ObjectId id;
	private String name;
	
	private List<Laptop> laptops;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Laptop> getLaptops() {
		return laptops;
	}

	public void setLaptops(List<Laptop> laptops) {
		this.laptops = laptops;
	}
}
