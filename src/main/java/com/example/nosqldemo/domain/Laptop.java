package com.example.nosqldemo.domain;

import org.bson.types.ObjectId;

public class Laptop {

	private ObjectId id;
	private String model;
	private double diagonal;
	private boolean ultrabook = false;
	private Manufacturer manufacturer;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public double getDiagonal() {
		return diagonal;
	}

	public void setDiagonal(double diagonal) {
		this.diagonal = diagonal;
	}

	public boolean isUltrabook() {
		return ultrabook;
	}

	public void setUltrabook(boolean ultrabook) {
		this.ultrabook = ultrabook;
	}

	public Manufacturer getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(Manufacturer manufacturer) {
		this.manufacturer = manufacturer;
	}
}
