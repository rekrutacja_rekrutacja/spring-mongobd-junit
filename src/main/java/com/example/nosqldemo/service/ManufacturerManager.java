package com.example.nosqldemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.nosqldemo.domain.Manufacturer;
import com.example.nosqldemo.repository.ManufacturerRepository;

@Component
public class ManufacturerManager {

	@Autowired
	private ManufacturerRepository manufacturerRepository;

	public void addManufacturer(Manufacturer manufacturer) {
		manufacturerRepository.save(manufacturer);
	}

	public Manufacturer getManufactuter(String name) {
		return manufacturerRepository.findByName(name);
	}

	public void deleteAllManufacturers() {
		manufacturerRepository.deleteAll();
	}
}
