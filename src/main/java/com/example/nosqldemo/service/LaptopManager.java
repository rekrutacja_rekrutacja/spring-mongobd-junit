package com.example.nosqldemo.service;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.nosqldemo.domain.Laptop;
import com.example.nosqldemo.domain.Manufacturer;
import com.example.nosqldemo.repository.LaptopRepository;

@Component
public class LaptopManager {

	@Autowired
	private LaptopRepository laptopRepository;

	public void addLaptop(Laptop laptop) {
		laptopRepository.save(laptop);
	}

	public Laptop getLaptopById(ObjectId id) {
		return laptopRepository.findById(id);
	}

	public List<Laptop> getLaptopsByModel(String model) {
		return laptopRepository.findByModel(model);
	}

	public List<Laptop> getLaptopsByManufacturerName(String manufacturerName) {
		return laptopRepository.findByManufacturerName(manufacturerName);
	}

	public List<Laptop> getLaptopsByManufacturer(Manufacturer manufacturer) {
		return laptopRepository.findByManufacturer(manufacturer);
	}

	public List<Laptop> getAllLaptops() {
		return (List<Laptop>) laptopRepository.findAll();
	}

	public void updateLaptop(Laptop laptop) {
		laptopRepository.save(laptop);
	}

	public void deleteLaptop(ObjectId id) {
		laptopRepository.delete(id);
	}

	public void deleteLaptop(Laptop laptop) {
		laptopRepository.delete(laptop);
	}

	public void deleteAllLaptops() {
		laptopRepository.deleteAll();
	}

	/*
	 * Unfortunately spring data doesn't provides any method to delete documents
	 * based on a query. And the @Query annotation is only for find documents.
	 * 
	 * W naszej wersji springa nawet zwykłe usuwanie nie działa.
	 */
	public void deleteLaptopByModelAndManufacturersName(String model,
			String manufacturersName) {
		List<Laptop> receivedLaptops = laptopRepository
				.findLaptopsByModelAndManufacturersName(model,
						manufacturersName);
		for (Laptop laptop : receivedLaptops)
			laptopRepository.delete(laptop);
	}
}
