package com.example.nosqldemo.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.example.nosqldemo.domain.Laptop;
import com.example.nosqldemo.domain.Manufacturer;
import com.example.nosqldemo.service.LaptopManager;
import com.example.nosqldemo.service.ManufacturerManager;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/beans.xml" })
public class ProductTest {

	private void cleanDatabase() {
		laptopManager.deleteAllLaptops();
		manufacturerManager.deleteAllManufacturers();
	}

	private List<ObjectId> fillInDatabaseWithLaptops() {
		List<ObjectId> listOfIds = new ArrayList<ObjectId>();

		Manufacturer manufacturer1 = new Manufacturer();
		manufacturer1.setName(MANUFACTURER_1);

		Manufacturer manufacturer2 = new Manufacturer();
		manufacturer2.setName(MANUFACTURER_2);

		Manufacturer manufacturer3 = new Manufacturer();
		manufacturer3.setName(MANUFACTURER_3);

		Laptop laptopToAdd1 = new Laptop();
		laptopToAdd1.setModel(MODEL_1);
		laptopToAdd1.setDiagonal(DIAGONAL_1);
		laptopToAdd1.setUltrabook(IS_ULTRABOOK_1);
		laptopToAdd1.setManufacturer(manufacturer1);
		laptopManager.addLaptop(laptopToAdd1);
		listOfIds.add(laptopToAdd1.getId());

		Laptop laptopToAdd2 = new Laptop();
		laptopToAdd2.setModel(MODEL_2);
		laptopToAdd2.setDiagonal(DIAGONAL_2);
		laptopToAdd2.setUltrabook(IS_ULTRABOOK_2);
		laptopToAdd2.setManufacturer(manufacturer2);
		laptopManager.addLaptop(laptopToAdd2);
		listOfIds.add(laptopToAdd2.getId());

		Laptop laptopToAdd3 = new Laptop();
		laptopToAdd3.setModel(MODEL_3);
		laptopToAdd3.setDiagonal(DIAGONAL_3);
		laptopToAdd3.setUltrabook(IS_ULTRABOOK_3);
		laptopToAdd3.setManufacturer(manufacturer3);
		laptopManager.addLaptop(laptopToAdd3);
		listOfIds.add(laptopToAdd3.getId());

		return listOfIds;
	}

	@Autowired
	LaptopManager laptopManager;
	@Autowired
	ManufacturerManager manufacturerManager;

	private final double EPSILON = 0.001;

	private final String MODEL_1 = "abc123";
	private final String MODEL_2 = "12-34-56-xyz";
	private final String MODEL_3 = "r2d2";
	private final String MODEL_4 = "edited";

	private final double DIAGONAL_1 = 13.3;
	private final double DIAGONAL_2 = 15.6;
	private final double DIAGONAL_3 = 17.3;

	private final boolean IS_ULTRABOOK_1 = true;
	private final boolean IS_ULTRABOOK_2 = false;
	private final boolean IS_ULTRABOOK_3 = false;

	private final String MANUFACTURER_1 = "ASUS";
	private final String MANUFACTURER_2 = "HP";
	private final String MANUFACTURER_3 = "MSI";

	@Test
	public void checkLaptopAdding() {
		Manufacturer manufacturer1 = new Manufacturer();
		manufacturer1.setName(MANUFACTURER_1);

		Manufacturer manufacturer2 = new Manufacturer();
		manufacturer2.setName(MANUFACTURER_2);

		Manufacturer manufacturer3 = new Manufacturer();
		manufacturer3.setName(MANUFACTURER_3);

		Laptop laptopToAdd1 = new Laptop();
		laptopToAdd1.setModel(MODEL_1);
		laptopToAdd1.setDiagonal(DIAGONAL_1);
		laptopToAdd1.setUltrabook(IS_ULTRABOOK_1);
		laptopToAdd1.setManufacturer(manufacturer1);
		laptopManager.addLaptop(laptopToAdd1);

		Laptop laptopToAdd2 = new Laptop();
		laptopToAdd2.setModel(MODEL_2);
		laptopToAdd2.setDiagonal(DIAGONAL_2);
		laptopToAdd2.setUltrabook(IS_ULTRABOOK_2);
		laptopToAdd2.setManufacturer(manufacturer2);
		laptopManager.addLaptop(laptopToAdd2);

		Laptop laptopToAdd3 = new Laptop();
		laptopToAdd3.setModel(MODEL_3);
		laptopToAdd3.setDiagonal(DIAGONAL_3);
		laptopToAdd3.setUltrabook(IS_ULTRABOOK_3);
		laptopToAdd3.setManufacturer(manufacturer3);
		laptopManager.addLaptop(laptopToAdd3);

		List<Laptop> allLaptops = laptopManager.getAllLaptops();
		assertEquals(3, allLaptops.size());

		Laptop firstLaptop = allLaptops.get(0);
		assertEquals(MODEL_1, firstLaptop.getModel());
		assertEquals(DIAGONAL_1, firstLaptop.getDiagonal(), EPSILON);
		assertEquals(IS_ULTRABOOK_1, firstLaptop.isUltrabook());
		assertEquals(MANUFACTURER_1, firstLaptop.getManufacturer().getName());

		cleanDatabase();
	}

	@Test
	public void checkLaptopFinding() {
		List<ObjectId> listOfIds = fillInDatabaseWithLaptops();

		Laptop receivedLaptop = laptopManager.getLaptopById(listOfIds.get(1));
		assertEquals(MODEL_2, receivedLaptop.getModel());
		assertEquals(DIAGONAL_2, receivedLaptop.getDiagonal(), EPSILON);
		assertEquals(IS_ULTRABOOK_2, receivedLaptop.isUltrabook());
		assertEquals(MANUFACTURER_2, receivedLaptop.getManufacturer().getName());

		cleanDatabase();
	}

	@Test
	public void checkAllLaptopsFinding() {
		fillInDatabaseWithLaptops();

		List<Laptop> allLaptops = laptopManager.getAllLaptops();
		assertEquals(3, allLaptops.size());

		Laptop receivedLaptop1 = allLaptops.get(0);
		assertEquals(MODEL_1, receivedLaptop1.getModel());
		assertEquals(DIAGONAL_1, receivedLaptop1.getDiagonal(), EPSILON);
		assertEquals(IS_ULTRABOOK_1, receivedLaptop1.isUltrabook());
		assertEquals(MANUFACTURER_1, receivedLaptop1.getManufacturer()
				.getName());

		Laptop receivedLaptop2 = allLaptops.get(1);
		assertEquals(MODEL_2, receivedLaptop2.getModel());
		assertEquals(DIAGONAL_2, receivedLaptop2.getDiagonal(), EPSILON);
		assertEquals(IS_ULTRABOOK_2, receivedLaptop2.isUltrabook());
		assertEquals(MANUFACTURER_2, receivedLaptop2.getManufacturer()
				.getName());

		Laptop receivedLaptop3 = allLaptops.get(2);
		assertEquals(MODEL_3, receivedLaptop3.getModel());
		assertEquals(DIAGONAL_3, receivedLaptop3.getDiagonal(), EPSILON);
		assertEquals(IS_ULTRABOOK_3, receivedLaptop3.isUltrabook());
		assertEquals(MANUFACTURER_3, receivedLaptop3.getManufacturer()
				.getName());

		cleanDatabase();
	}

	@Test
	public void checkLaptopUpdating() {
		List<ObjectId> listOfIds = fillInDatabaseWithLaptops();

		Laptop receivedLaptop1 = laptopManager.getLaptopById(listOfIds.get(0));
		receivedLaptop1.setModel(MODEL_4);
		laptopManager.updateLaptop(receivedLaptop1);

		assertEquals(3, laptopManager.getAllLaptops().size());

		Laptop receivedLaptop2 = laptopManager.getLaptopById(receivedLaptop1
				.getId());
		assertEquals(MODEL_4, receivedLaptop2.getModel());

		cleanDatabase();
	}

	@Test
	public void checkLaptopDeleting() {
		List<ObjectId> listOfIds = fillInDatabaseWithLaptops();

		ObjectId idToDelete = listOfIds.get(0);
		laptopManager.deleteLaptop(idToDelete);
		assertEquals(2, laptopManager.getAllLaptops().size());
		assertNull(laptopManager.getLaptopById(idToDelete));

		cleanDatabase();
	}

	@Test
	public void checkLaptopByModelFinding() {
		fillInDatabaseWithLaptops();
		fillInDatabaseWithLaptops();

		List<Laptop> allOfTheGivenModelLaptops = laptopManager
				.getLaptopsByModel(MODEL_1);
		assertEquals(2, allOfTheGivenModelLaptops.size());

		for (Laptop laptop : allOfTheGivenModelLaptops)
			assertEquals(MODEL_1, laptop.getModel());

		cleanDatabase();
	}

	@Test
	public void checkLaptopByManufacturerFinding() {
		fillInDatabaseWithLaptops();
		fillInDatabaseWithLaptops();

		List<Laptop> allManufacturersLaptops = laptopManager
				.getLaptopsByManufacturerName(MANUFACTURER_1);

		assertEquals(2, allManufacturersLaptops.size());
		for (Laptop laptop : allManufacturersLaptops)
			assertEquals(MANUFACTURER_1, laptop.getManufacturer().getName());

		cleanDatabase();
	}

	@Test
	public void checkLaptopByModelAndManufacturerDeleting() {

		Manufacturer manufacturer1 = new Manufacturer();
		manufacturer1.setName(MANUFACTURER_1);

		Manufacturer manufacturer2 = new Manufacturer();
		manufacturer2.setName(MANUFACTURER_2);

		Laptop laptopToAdd1 = new Laptop();
		laptopToAdd1.setModel(MODEL_1);
		laptopToAdd1.setDiagonal(DIAGONAL_1);
		laptopToAdd1.setUltrabook(IS_ULTRABOOK_1);
		laptopToAdd1.setManufacturer(manufacturer1);
		laptopManager.addLaptop(laptopToAdd1);

		Laptop laptopToAdd2 = new Laptop();
		laptopToAdd2.setModel(MODEL_1);
		laptopToAdd2.setDiagonal(DIAGONAL_1);
		laptopToAdd2.setUltrabook(IS_ULTRABOOK_1);
		laptopToAdd2.setManufacturer(manufacturer2);
		laptopManager.addLaptop(laptopToAdd2);

		laptopManager.deleteLaptopByModelAndManufacturersName(MODEL_1,
				MANUFACTURER_1);

		List<Laptop> allLaptops = laptopManager.getAllLaptops();

		assertEquals(1, allLaptops.size());
		assertEquals(MANUFACTURER_2, allLaptops.get(0).getManufacturer()
				.getName());

		cleanDatabase();
	}

	@Test
	public void checkLaptopByModelAndManufacturerDeleting2() {

		Manufacturer manufacturer1 = new Manufacturer();
		manufacturer1.setName(MANUFACTURER_1);

		Manufacturer manufacturer2 = new Manufacturer();
		manufacturer2.setName(MANUFACTURER_2);

		Laptop laptopToAdd1 = new Laptop();
		laptopToAdd1.setModel(MODEL_1);
		laptopToAdd1.setDiagonal(DIAGONAL_1);
		laptopToAdd1.setUltrabook(IS_ULTRABOOK_1);
		laptopToAdd1.setManufacturer(manufacturer1);
		laptopManager.addLaptop(laptopToAdd1);

		Laptop laptopToAdd2 = new Laptop();
		laptopToAdd2.setModel(MODEL_1);
		laptopToAdd2.setDiagonal(DIAGONAL_1);
		laptopToAdd2.setUltrabook(IS_ULTRABOOK_1);
		laptopToAdd2.setManufacturer(manufacturer2);
		laptopManager.addLaptop(laptopToAdd2);

		laptopManager.deleteLaptopByModelAndManufacturersName(MODEL_1,
				MANUFACTURER_1);

		List<Laptop> allLaptops = laptopManager.getAllLaptops();

		assertEquals(1, allLaptops.size());
		assertEquals(MANUFACTURER_2, allLaptops.get(0).getManufacturer()
				.getName());

		cleanDatabase();
	}

}
